package com.roll.it.android;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.roll.Handlers.GlobalData;
import com.roll.Scenes.MainMenu;
import com.roll.it.MazeGame;

public class AndroidLauncher extends AndroidApplication {

    public MazeGame mazeGame = new MazeGame();
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(mazeGame, config);
	}

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            super.onPause();
        }

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!(GlobalData.getInstance().getSceneManager().getCurrentlyRunning().getClass() == MainMenu.class)) {
                GlobalData.getInstance().getSceneManager().setCurrentlyRunning(new MainMenu());
                return false;
            } else {
                exit();
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
