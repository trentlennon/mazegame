package com.roll.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.roll.Handlers.GlobalData;
import com.roll.entities.MySprite;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Trent on 5/22/2014.
 */

public class WinScreen extends Stage {

    private OrthographicCamera camera;

    public WinScreen() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800,400);
        FitViewport viewport = new FitViewport(800,400, camera);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.setViewport(viewport);

        Table table = new Table();
        table.add(new Image(new Texture(Gdx.files.internal("gamewon.png"))));
        table.setFillParent(true);
        this.addActor(table);

        this.addAction(sequence(delay(1.5f),fadeOut(1.0f), run(new Runnable() {
            @Override
            public void run() {
                Gdx.app.log("Sequence","Exit");
                GlobalData.getInstance().sceneManager.setCurrentlyRunning(new MainMenu());
            }
        })));
    }

    @Override
    public void act() {
        Gdx.input.setInputProcessor(this);
        super.act();
    }

    @Override
    public void draw() {
        super.draw();
    }

    private class MainMenuUITable extends Table {
        Texture logo = new Texture(Gdx.files.internal("gamewon.png"));

        MySprite sLogo;

        public MainMenuUITable() {
            sLogo = new MySprite(logo);
            sLogo.setWidth(sLogo.getWidth() * 2);
            sLogo.setHeight(sLogo.getHeight() * 2);
            setFillParent(true);

        }
    }
}