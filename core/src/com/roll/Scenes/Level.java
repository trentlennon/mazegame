package com.roll.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.google.gson.Gson;
import com.roll.Handlers.GlobalData;
import com.roll.editor.HudStage;
import com.roll.entities.End;
import com.roll.entities.Hole;
import com.roll.entities.Player;
import com.roll.entities.Wall;

import java.util.Iterator;
import java.util.Stack;

public class Level extends Stage {

    MyContactListener contactListener = new MyContactListener();

    private Stack<Message> messageStack = new Stack<Message>();

    World world = new World(new Vector2(0, 0), true);

    private Vector2 startPosition;

    private SpriteBatch batch = new SpriteBatch();

    Box2DDebugRenderer debugRenderer;
    OrthographicCamera camera;

    BodyDef def = new BodyDef();

    Player player;
    Wall wall;
    Hole hole;
    End end;

    static final float BOX_STEP=1/60f;
    static final int BOX_VELOCITY_ITERATIONS=6;
    static final int BOX_POSITION_ITERATIONS=2;

    public Level() {
        //Creates the camera. Kind of has to be first...
        initialize();

        FileHandle handler = Gdx.files.external("data/roll.it/testLevel.json");

        if(!handler.exists()){
            Gdx.app.log("System File","File does not exist.");
        }
        else {
            String json = handler.readString();
            bodiesFromJson(json);
        }
    }

    public Level(String jsonLevel) {
        jsonLevel = jsonLevel.replaceAll("\\s+","");
        initialize();
        bodiesFromJson(jsonLevel);
    }

    private void bodiesFromJson(String json) {
        Gson gson = new Gson();
        HudStage.jsonActor[] jsonActors;
        jsonActors = gson.fromJson(json, HudStage.jsonActor[].class);

        for (HudStage.jsonActor jsonActor : jsonActors) {
            if (jsonActor.type == HudStage.Type.WALL) {
                Vector3 pos = camera.unproject(jsonActor.position);

                def.position.set(new Vector2(pos.x, -pos.y));
                wall = new Wall(world.createBody(def));
                this.addActor(wall);
            } else if (jsonActor.type == HudStage.Type.START) {

                Vector3 pos = camera.unproject(jsonActor.position);
                startPosition = new Vector2(pos.x, -pos.y);

                def.position.set(new Vector2(startPosition.x, startPosition.y));
                player = new Player(world.createBody(def));
                this.addActor(player);
            } else if (jsonActor.type == HudStage.Type.HOLE) {
                Vector3 pos = camera.unproject(jsonActor.position);

                def.position.set(new Vector2(pos.x, -pos.y));
                hole = new Hole(world.createBody(def));
                this.addActor(hole);
            } else if (jsonActor.type == HudStage.Type.END) {
                Vector3 pos = camera.unproject(jsonActor.position);

                def.position.set(new Vector2(pos.x, -pos.y));
                end = new End(world.createBody(def));
                this.addActor(end);
            }
        }
    }

    private void initialize() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        float worldWidth = 60;
        float worldHeight = 40;
        camera.setToOrtho(false, worldWidth, worldHeight);
        camera.update();

        debugRenderer = new Box2DDebugRenderer(true, true, false, true, true, true);

        ExtendViewport viewport = new ExtendViewport(worldWidth, worldHeight, camera);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.setViewport(viewport);

        //Sets up the contact listener for the world.
        world.setContactListener(contactListener);
    }

    private void update(float delta) {
//        player.update();
        world.step(delta, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
    }

    double accumulator = 0.0;
    //Prevent spiral of doom/death
    final int MAX_PHYSICS_FRAMES = 3;
    int framesRenderedSoFar = 0;
    boolean win = false;

    @Override
    public void draw() {

        while(!messageStack.isEmpty())
        {
            Message mess = messageStack.pop();
            if(mess == Message.RESET_PLAYER)
            {
                resetPlayer();
            }
            if(mess == Message.GAME_WON)
            {
                win = true;
            }
        }

        if(win) {
            Gdx.app.log("Level Change to win screen","WIN!");
            GlobalData.getInstance().sceneManager.setCurrentlyRunning(new WinScreen());
        }
        playLoop();
    }

    private void playLoop() {
        accumulator += Gdx.graphics.getDeltaTime();
        while (accumulator >= BOX_STEP && framesRenderedSoFar < MAX_PHYSICS_FRAMES ) {
            update(BOX_STEP);

            player.update();
            accumulator -= BOX_STEP;
            framesRenderedSoFar++;
        }
        framesRenderedSoFar = 0;

        followThePlayer();

        camera.update();
        //debugRenderer.render(world, camera.combined);

        Iterator<Actor> actors = getActors().iterator();
        getSpriteBatch().begin();
        while(actors.hasNext()) {
            actors.next().draw(getSpriteBatch(), 0.0f);
        }
        getSpriteBatch().end();
    }

    private void followThePlayer() {
        camera.position.set(player.getBody().getPosition().x, player.getBody().getPosition().y, 0.0f);
    }

    private void resetPlayer() {
        player.getBody().setTransform(startPosition, player.getBody().getAngle());
        player.getBody().setLinearVelocity(0,0);
    }

    @Override
    public Batch getSpriteBatch() {
        batch.setProjectionMatrix(camera.combined);
        return batch;
    }

    private class MyContactListener implements ContactListener {

        @Override
        public void beginContact(Contact contact) {
            final Fixture x1 = contact.getFixtureA();
            final Fixture x2 = contact.getFixtureB();

            if(x1.getBody().getUserData() == HudStage.Type.PLAYER && x2.getBody().getUserData() == HudStage.Type.WALL ||
                x2.getBody().getUserData() == HudStage.Type.PLAYER && x1.getBody().getUserData() == HudStage.Type.WALL) {
                Gdx.app.log("Collision Detection","Player and wall collided");
            }
            else if(x1.getBody().getUserData() == HudStage.Type.PLAYER && x2.getBody().getUserData() == HudStage.Type.HOLE ||
                    x2.getBody().getUserData() == HudStage.Type.PLAYER && x1.getBody().getUserData() == HudStage.Type.HOLE) {
                Gdx.app.log("Collision Detection", "Player and hole collided");
                messageStack.push(Message.RESET_PLAYER);
            }
            else if(x1.getBody().getUserData() == HudStage.Type.PLAYER && x2.getBody().getUserData() == HudStage.Type.END ||
                    x2.getBody().getUserData() == HudStage.Type.PLAYER && x1.getBody().getUserData() == HudStage.Type.END) {
                Gdx.app.log("Collision Detection", "Player and end collided");
                messageStack.push(Message.GAME_WON);
            }
        }

        @Override
        public void endContact(Contact contact) {
            //Could check which is which.
        }

        @Override
        public void preSolve(Contact contact, Manifold oldManifold) {

        }

        @Override
        public void postSolve(Contact contact, ContactImpulse impulse) {

        }
    }

    private enum Message {
        RESET_PLAYER,
        GAME_WON
    }
}