package com.roll.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.roll.Handlers.GlobalData;

/**
 * Created by Trent on 6/26/2014.
 */

//TODO: just add the scene manager to the GlobalData object.
public class StageSelect extends Stage {
    private Table levelTables;
    //Create local reference to the skin file.
    private Skin mySkin = GlobalData.getInstance().manager.get("ui/uiskin.json",Skin.class);
    private ExtendViewport screenViewport;
    private OrthographicCamera camera;
    private TextButton level1_1;
    private TextButton level1_2;
    public StageSelect() {
        super();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 400,240);

        screenViewport = new ExtendViewport(400,240,camera);
        screenViewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.setViewport(screenViewport);

        levelTables = new Table();
        levelTables.setFillParent(true);
        level1_1 = new TextButton("Level 1-1", mySkin);
        levelTables.add(level1_1);
        level1_1.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.log("Touch Event","Level Selector");
                //            Read the JSON from the file.
                String jsonLevel = Gdx.files.internal("Levels/1-1.json").readString().replaceAll("\\s+","");
                GlobalData.getInstance().sceneManager.setCurrentlyRunning(new Level(jsonLevel));
//            Push the new stage on to the stack.
            }
        });

        level1_2 = new TextButton("Level 1-2", mySkin);
        levelTables.add(level1_2);
        level1_2.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.log("Touch Event","Level Selector");
                //            Read the JSON from the file.
                String jsonLevel = Gdx.files.internal("Levels/1-2.json").readString().replaceAll("\\s+","");
//                GlobalData.getInstance().getSceneManager().put("Level", new Level(jsonLevel));
                GlobalData.getInstance().sceneManager.setCurrentlyRunning(new Level(jsonLevel));
//            Push the new stage on to the stack.
            }
        });

        levelTables.row();

        TextButton PlayerLevels = new TextButton("Player Levels", mySkin);
        PlayerLevels.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                showLoadDialog();
            }
        });
        levelTables.add(PlayerLevels);

        this.addActor(levelTables);
    }

    public void showLoadDialog() {
        this.addActor(new Window("Load", mySkin) {
            {
                setFillParent(true);

                TextButton okButton = new TextButton("OK", mySkin);
                TextButton cancelButton = new TextButton("CANCEL", mySkin);
                cancelButton.addListener(new InputListener() {
                    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                        return true;
                    }
                    public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                        hide();
                    }
                });

                FileHandle dirHandler = Gdx.files.external("data/roll.it");
                if(dirHandler.list().length == 0 ) {
                    Gdx.app.log("Empty","Nothing to report.");
                    add(new Label("There are no levels currently saved here.", mySkin));
                    row();
                }
                else {
                    //I just set the list of the items.
                    final List<String> objects = new List<String>(mySkin);
                    Array<String> names = new Array<String>();

                    for(int i = 0; i < dirHandler.list().length; i ++) {
                        names.add(dirHandler.list()[i].nameWithoutExtension());
                    }
                    Gdx.app.log("names", names.toString());
                    objects.setItems(names);

                    okButton.addListener(new InputListener() {
                        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                            return true;
                        }
                        public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                            String loadThisLevel = objects.getSelected();

                            FileHandle handler = Gdx.files.external("data/roll.it/" + loadThisLevel + ".json");
                            String json = handler.readString();
                            GlobalData.getInstance().sceneManager.setCurrentlyRunning(new Level(json));
                        }
                    });
                    add(objects);
                    row();
                    add(okButton);
                }
                //Always make sure the dialog can exit.
                add(cancelButton);
            }

            private void hide() {
                this.remove();
            }
        });
    }

    @Override
    public void draw() {
        Gdx.input.setInputProcessor(this);
        super.draw();
    }
}
