package com.roll.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.roll.Handlers.GlobalData;
import com.roll.editor.LevelEditor;
import com.roll.entities.MySprite;

/**
 * Created by Trent on 5/22/2014.
 */

public class MainMenu extends Stage {
    private MainMenuUITable menuUi;
    private OrthographicCamera camera;

    public MainMenu() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 400,240);
        FitViewport viewport = new FitViewport(400,240, camera);

        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        this.setViewport(viewport);
        menuUi = new MainMenuUITable();
        addActor(menuUi);
    }

    @Override
    public void act(float delta) {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void draw() {
        super.draw();
    }

    private class MainMenuUITable extends Table {
        TextButton editorButton;
        TextButton startButton;

        Texture logo = GlobalData.getInstance().Logo;

        MySprite sLogo;

        public MainMenuUITable() {

            editorButton = new TextButton("Level Editor", GlobalData.getInstance().mySkin);
            startButton = new TextButton("Start the Game!", GlobalData.getInstance().mySkin);

            sLogo = new MySprite(logo);
            setFillParent(true);
            add(sLogo);
            row();
            add(startButton);
            row();
            add(editorButton);


            editorButton.addListener(new InputListener() {
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }


                 public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    GlobalData.getInstance().getSceneManager().setCurrentlyRunning(new LevelEditor());
                }
            });
            startButton.addListener(new InputListener() {
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }


                 public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    GlobalData.getInstance().getSceneManager().setCurrentlyRunning(new StageSelect());
                }
            });

        }
    }
}