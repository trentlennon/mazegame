 package com.roll.Handlers;

 import com.badlogic.gdx.Gdx;
 import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

 /**
 * Created by Trent on 6/5/2014.
 */
public class GlobalData {
    private static GlobalData instance = null;
    public SceneManager sceneManager;

    public SceneManager getSceneManager() {
        return sceneManager;
    }
    protected GlobalData() {
        manager = new AssetManager();
        sceneManager = new SceneManager();
    }

    public static GlobalData getInstance() {
        if(instance==null) {
            instance = new GlobalData();

            WallTexture = new Texture(Gdx.files.internal("wall.png"));
            BallTexture = new Texture(Gdx.files.internal("ball.png"));
            HoleTexture = new Texture(Gdx.files.internal("hole.png"));
            EndTexture = new Texture(Gdx.files.internal("end.png"));
            Logo = new Texture(Gdx.files.internal("logo.png"));
            StartTexture = new Texture(Gdx.files.internal("start.png"));
            mySkin = new Skin(Gdx.files.internal("ui/uiskin.json"));
            editorButtonTexture = new Texture(Gdx.files.internal("editorButton.png"));
            startButtonTexture = new Texture(Gdx.files.internal("startButton.png"));
        }
        return instance;
    }
    public void ReloadTextures() {
        WallTexture = new Texture(Gdx.files.internal("wall.png"));
        BallTexture = new Texture(Gdx.files.internal("ball.png"));
        HoleTexture = new Texture(Gdx.files.internal("hole.png"));
        EndTexture = new Texture(Gdx.files.internal("end.png"));
        StartTexture = new Texture(Gdx.files.internal("start.png"));
        Logo = new Texture(Gdx.files.internal("logo.png"));
        editorButtonTexture = new Texture(Gdx.files.internal("editorButton.png"));
        startButtonTexture = new Texture(Gdx.files.internal("startButton.png"));
        mySkin = new Skin(Gdx.files.internal("ui/uiskin.json"));
    }

    public static Texture editorButtonTexture;
    public static Texture startButtonTexture;
    public static AssetManager manager;
    public static Texture WallTexture;
    public static Texture BallTexture;
    public static Texture HoleTexture;
    public static Texture EndTexture;
    public static Texture StartTexture;
    public static Texture Logo;
    public static Skin mySkin;
}
