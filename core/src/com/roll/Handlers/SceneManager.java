package com.roll.Handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.roll.Scenes.Level;
import com.roll.Scenes.MainMenu;
import com.roll.Scenes.StageSelect;
import com.roll.Scenes.WinScreen;
import com.roll.editor.LevelEditor;

/**
 * Created by Trent on 5/22/2014.
 */

public class SceneManager {
    private Stage currentlyRunning;
    private Music music;

    public SceneManager() {
        currentlyRunning = null;
        music = null;
    }

    public void setCurrentlyRunning(Stage runThis) {
        Gdx.input.setInputProcessor(runThis);//Needs to be here.
        playNewMusic(runThis);
        currentlyRunning = runThis;
    }
    public Stage getCurrentlyRunning() {
        return currentlyRunning;
    }

    public void ChangeTo(Stage changeTo) {
        Gdx.input.setInputProcessor(changeTo);//Needs to be here.

        playNewMusic(changeTo);
        setCurrentlyRunning(changeTo);
    }

    public void draw() {
        getCurrentlyRunning().draw();
    }

    public void act(float delta) {
        getCurrentlyRunning().act(delta);
    }

    public void playNewMusic(Stage stageName) {

        if (music != null) {
            music.stop();
            music.dispose();
        }

        if (stageName.getClass() == MainMenu.class) {
            music = Gdx.audio.newMusic(Gdx.files.internal("Title-Pamgaea.mp3"));
        } else if (stageName.getClass() == LevelEditor.class) {
            music = Gdx.audio.newMusic(Gdx.files.internal("Editor-Electrodoodle.mp3"));
        } else if (stageName.getClass() == Level.class || stageName.getClass()== StageSelect.class) {
            music = Gdx.audio.newMusic(Gdx.files.internal("Gameplay-Show Your Moves.mp3"));
        } else if (stageName.getClass() == WinScreen.class || stageName.getClass()== StageSelect.class) {
            music = Gdx.audio.newMusic(Gdx.files.internal("YouWin-Cheer.mp3"));
        }

        if (music != null) {
            music.play();
            music.setLooping(true);
        }
    }
}
