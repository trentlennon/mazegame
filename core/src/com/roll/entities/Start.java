package com.roll.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by kims3 on 5/23/2014.
 */
public class Start extends Actor {
    private Texture texture;

    public Start(Texture text) {
        this.texture = text;
        this.setWidth(texture.getWidth());
        this.setHeight(texture.getHeight());
        this.setX(0);
        this.setY(0);
    }

    @Override
    public void draw(Batch batch, float delta) {
        batch.draw(texture, this.getX(), this.getY());
    }
}
