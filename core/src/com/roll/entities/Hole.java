package com.roll.entities;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.roll.Handlers.GlobalData;
import com.roll.editor.HudStage;

/**
 * Created by Trent on 6/24/2014.
 */
public class Hole extends B2DSprite {

    public Hole(Body body) {
        super(body);

        bodyShape = new CircleShape();
        bodyShape.setRadius(0.5f);
        sprite.setTexture(GlobalData.getInstance().HoleTexture);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = bodyShape;
        fixtureDef.isSensor = true;

        boxWidth = bodyShape.getRadius() * 2;
        boxHeight = bodyShape.getRadius() * 2;
        sprite.setSize(2,2);

        getBody().createFixture(fixtureDef);
        body.setUserData(HudStage.Type.HOLE);
    }
}