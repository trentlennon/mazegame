package com.roll.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.roll.Handlers.GlobalData;
import com.roll.editor.HudStage;

public class Player extends B2DSprite {

    private boolean setToInitialPosition = false;
    private Vector2 initialPosition;
    public Player(Body body) {
        super(body);
        body.setType(BodyDef.BodyType.DynamicBody);     //Make this a kinematic body.

        initialPosition = body.getPosition();

        bodyShape = new CircleShape();
        bodyShape.setRadius(1.0f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = bodyShape;
        fixtureDef.density = 2.0f;
        fixtureDef.friction = 0.0f;
        fixtureDef.restitution = 0.75f;

        getBody().createFixture(fixtureDef);
        body.setUserData(HudStage.Type.PLAYER);
        sprite.setTexture(GlobalData.getInstance().BallTexture);

        boxWidth = bodyShape.getRadius() * 2;
        boxHeight = bodyShape.getRadius() * 2;

        sprite.setSize(2,2);
        toBack(); //Ensures it is drawn on top.
    }

    private float dirX = 0.0f;
    private float dirY = 0.0f;

    final float SPEED = 4.0f;
    public void update() {
        dirX = Gdx.input.getAccelerometerY() * SPEED;
        dirY = Gdx.input.getAccelerometerX() * SPEED ;
        getBody().applyForceToCenter(new Vector2(dirX * 9.8f, -dirY * 9.8f), true);
    }
}
