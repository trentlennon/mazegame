package com.roll.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Trent on 6/14/2014.
 */
public class MySprite extends Actor {
    private Texture sprite;

    public MySprite(Texture texture) {
        sprite = texture;
        setBounds(0,0, sprite.getWidth(), sprite.getHeight());
    }

    @Override
    public void draw(Batch batch, float parentFloat) {
        batch.draw(sprite, getX(), getY(), getWidth(), getHeight());
    }
}