package com.roll.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Attaches animated sprites to box2d bodies
 */
public class B2DSprite extends Actor {
    protected Sprite sprite;
    protected Body body;
    protected float boxWidth;
    protected float boxHeight;
    protected Shape bodyShape;

    public B2DSprite(Body body) {
        sprite = new Sprite();
        sprite.setOrigin(body.getPosition().x, body.getPosition().y);
        this.body = body;
    }

    protected void setTexture(Texture texture) {
        sprite.setTexture(texture);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //Texture never actually gets set...
        batch.draw(sprite.getTexture(), body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2, sprite.getWidth(), sprite.getHeight());
    }
    public Body getBody() { return body; }

    public void setPosition(float x, float y) {
        sprite.setX(x);
        sprite.setY(y);
    }
}