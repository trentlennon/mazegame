package com.roll.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.roll.Handlers.GlobalData;

/**
 * Created by Trent on 5/23/2014.
 */
public class TransitionButton extends Actor {
    private Texture texture;
    private Stage stageToChangeTo;

    public TransitionButton(Stage stageObject, Texture texture) {
        this.texture = texture;
        stageToChangeTo = stageObject;

        this.setWidth(this.texture.getWidth());
        this.setHeight(this.texture.getHeight());

        this.setX(Gdx.graphics.getWidth() - 100);
        this.setY(Gdx.graphics.getHeight() - 100);

        this.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                GlobalData.getInstance().getSceneManager().setCurrentlyRunning(stageToChangeTo);
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }
}

