package com.roll.entities;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.roll.Handlers.GlobalData;
import com.roll.editor.HudStage;

public class Wall extends B2DSprite{
    private final float BOX_WIDTH = 2f;
    private final float BOX_HEIGHT = 2f;

    public Wall(Body body) {
        super(body);
        PolygonShape box = new PolygonShape();
        sprite.setTexture(GlobalData.getInstance().WallTexture);
        box.setAsBox(BOX_WIDTH, BOX_HEIGHT);
        body.createFixture(box, 0.0f);
        boxWidth = BOX_WIDTH * 2;
        boxHeight = BOX_HEIGHT * 2;
        body.setUserData(HudStage.Type.WALL);
        sprite.setSize(BOX_WIDTH * 2, BOX_HEIGHT * 2);
    }
}
