package com.roll.it;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.roll.Handlers.GlobalData;
import com.roll.Scenes.MainMenu;

public class MazeGame extends ApplicationAdapter {
    GlobalData data;

	@Override
	public void create () {
        data = GlobalData.getInstance();

        //TODO: Load assets into the asset manager
        data.manager.load("ui/uiskin.json", Skin.class);
        data.manager.finishLoading();

        //Test something.
        data.manager.load("start.png",Texture.class);
        data.manager.load("end.png",Texture.class);
        data.manager.load("wall.png",Texture.class);
        data.manager.load("wallButton.png",Texture.class);
        data.manager.finishLoading();

        //level = new Level();

        data.sceneManager.setCurrentlyRunning(new MainMenu());
	}

    double _accumulator = 0.0;
    final float BOX_STEP = 1.0f/60.0f;
    int physicsFramesRenderedSoFar = 0;
    final int MAX_PHYSICS_FRAMES = 3;

	@Override
	public void render () {
        //Display loading screen if everything isn't loaded
            _accumulator += Gdx.graphics.getDeltaTime();

            while (_accumulator >= BOX_STEP && physicsFramesRenderedSoFar < MAX_PHYSICS_FRAMES) {

                data.sceneManager.act(BOX_STEP);
                _accumulator -= BOX_STEP;
                physicsFramesRenderedSoFar++;
            }

            physicsFramesRenderedSoFar = 0;
            GL20 gl = Gdx.graphics.getGL20();
            gl.glClear(gl.GL_COLOR_BUFFER_BIT);

            data.sceneManager.draw();
	}

    @Override
    public void pause () {

    }

    @Override
    public void resume () {
        GlobalData.getInstance().ReloadTextures();
    }
}