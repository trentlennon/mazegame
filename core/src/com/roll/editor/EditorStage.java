package com.roll.editor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.google.gson.Gson;
import com.roll.Handlers.GlobalData;
import com.roll.entities.MySprite;

/**
 * Created by Trent on 6/14/2014.
 */
public class EditorStage extends Stage {
    public float zoom = 1.0f;
    public MyGestureHandler gestureHandler;

    OrthographicCamera editorCamera;
    private MySprite end;
    private MySprite start;

    private Texture endTexture;
    private Texture startTexture;

    private Vector2 initialPosition;

    private boolean delete = false;

    public EditorStage() {

       gestureHandler = new MyGestureHandler();
       editorCamera = new OrthographicCamera();
       editorCamera.setToOrtho(false, 800,600);

       initialPosition = new Vector2(editorCamera.position.x, editorCamera.position.y);

       startTexture = GlobalData.getInstance().StartTexture;
       start = new MySprite(startTexture);
       start.setPosition(200,200);
       start.setBounds(start.getX(),start.getY(), start.getWidth(),start.getHeight());
       start.setName("start");
       start.setTouchable(Touchable.enabled);
       start.addListener(new MyInputListener(start) {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
       endTexture = GlobalData.getInstance().EndTexture;
       end = new MySprite(endTexture);
       end.setName("end");
       end.setBounds(end.getX(),end.getY(), end.getWidth(),end.getHeight());
       end.setTouchable(Touchable.enabled);
       end.setPosition(600,200);
       end.addListener(new MyInputListener(end) {
            //Overrides it so that the touch down will not delete the end flag.
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

       this.addActor(start);
       this.addActor(end);

       ExtendViewport viewport = new ExtendViewport(editorCamera.viewportWidth, editorCamera.viewportHeight, editorCamera);
       viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
       this.setViewport(viewport);

       createBoxOfWalls();
    }

    private void createBoxOfWalls() {
        Texture wallTexture = GlobalData.getInstance().WallTexture;
        for(int x = 0; x < 800; x+=(wallTexture.getWidth())) {
            for(int y = 0; y < 600; y+=(wallTexture.getHeight())) {

                final MySprite wall = new MySprite(wallTexture);
                wall.setName("wall");
                if ((x != 0  && x < 800-wall.getWidth()) && (y != 0 && y < 600-wall.getHeight())) {

                } else {
                    wall.setPosition(x, y);
                    wall.setBounds(wall.getX(), wall.getY(), wall.getWidth(), wall.getHeight());

                    wall.setTouchable(Touchable.enabled);
                    wall.addListener(new MyInputListener(wall));
                    this.addActor(wall);
                }
            }
        }
    }

    public MySprite addHole() {
        //TODO Something seems to be wrong with the whole globaldata assetManager thingy.
        //TODO Replace with reference to globaldata assetManager
        final MySprite sprite = new MySprite(GlobalData.getInstance().HoleTexture);
        sprite.setName("hole");
        sprite.setPosition(getViewport().getViewportX() + getCamera().viewportWidth / 2, getViewport().getViewportY() + getCamera().viewportHeight / 2);
        sprite.addListener(new MyInputListener(sprite));
        return sprite;
    }
    public MySprite addWall() {
        //TODO Replace with reference to globaldata assetManager
        final MySprite sprite = new MySprite(GlobalData.getInstance().WallTexture);
        sprite.setName("wall");
        sprite.setPosition(getViewport().getViewportX() + getCamera().viewportWidth / 2, getViewport().getViewportY() + getCamera().viewportHeight / 2);
        sprite.addListener(new MyInputListener(sprite));
        return sprite;
    }
    public void setDelete(boolean delete) {
        this.delete = delete;
    }
    public boolean shouldDelete() {
        return delete;
    }

    public void resetPosition() {
        editorCamera.position.x = initialPosition.x;
        editorCamera.position.y = initialPosition.y;
        zoom = 1;
    }

    @Override
    public void draw() {
        zoom = MathUtils.clamp(zoom, 1f, 10f);
        editorCamera.zoom = zoom;
        editorCamera.update();
        super.draw();
    }

    class MyGestureHandler implements GestureDetector.GestureListener {

        public float initialScale = 5.0f;

        @Override
        public boolean touchDown(float x, float y, int pointer, int button) {
            initialScale = zoom;
            return false;
        }

        @Override
        public boolean tap(float x, float y, int count, int button) {
            return false;
        }

        @Override
        public boolean longPress(float x, float y) {
            return false;
        }

        @Override
        public boolean fling(float velocityX, float velocityY, int button) {
            return false;
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            Vector3 unprojectedInitialPoint = new Vector3(x,y,0.0f);
            unprojectedInitialPoint = editorCamera.unproject(unprojectedInitialPoint);

            Vector3 unprojectedFinalPoint = new Vector3(x+deltaX,y+deltaY,0.0f);
            unprojectedFinalPoint = editorCamera.unproject(unprojectedFinalPoint);

            float projectedDistanceX = unprojectedFinalPoint.x - unprojectedInitialPoint.x;
            float projectedDistanceY = unprojectedFinalPoint.y - unprojectedInitialPoint.y;

            editorCamera.translate(-projectedDistanceX , -projectedDistanceY );

            return true;
        }

        @Override
        public boolean panStop(float x, float y, int pointer, int button) {
            return false;
        }

        @Override
        public boolean zoom (float originalDistance, float currentDistance) {
            float ratio = originalDistance / currentDistance;
            zoom = initialScale * ratio;
            return false;
        }

        @Override
        public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
            return false;
        }
    }

    /**
     * I want all of the objects to react to touch the same exact way. By doing it this way I
     * can have them all act according to the same rules.
     */
    public class MyInputListener extends InputListener {
        MySprite parent;
        public MyInputListener(MySprite parent) {
            this.parent = parent;
        }

        @Override
        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
            if(delete) {
                parent.remove();
            }
            return true;
        }

        @Override
        public void touchDragged (InputEvent event, float x, float y, int pointer) {
            parent.setPosition(parent.getX() + x,parent.getY() + y);
        }
    }

    //None of these will have touchInput.
    public void readLevel(String json) {
        FileHandle levelToLoad = Gdx.files.external("data/roll.it/" + json + ".json");
        String levelAsString = levelToLoad.readString();
        Gdx.app.log("Level as a String", levelAsString);

        Gson gson = new Gson();
        HudStage.jsonActor[] jsonActors;
        jsonActors = gson.fromJson(levelAsString, HudStage.jsonActor[].class);

        for (HudStage.jsonActor jsonActor : jsonActors) {
            Vector3 pos = editorCamera.unproject(jsonActor.position);
            MySprite sprite = null;

            switch (jsonActor.type ) {
                case WALL:
                    sprite = new MySprite(new Texture(Gdx.files.internal("wall.png")));
                    sprite.setName("wall");
                    break;
                case START:
                    sprite = new MySprite(new Texture(Gdx.files.internal("start.png")));
                    sprite.setName("start");
                    break;
                case HOLE:
                    sprite = new MySprite(new Texture(Gdx.files.internal("hole.png")));
                    sprite.setName("hole");
                    break;
                case END:
                    sprite = new MySprite(new Texture(Gdx.files.internal("end.png")));
                    sprite.setName("end");
                    break;
            }

            //Wrapped in case sprite is null.
            try {
                sprite.addListener((new MyInputListener(sprite)));
                sprite.setPosition(pos.x, -pos.y);
                addActor(sprite);
            } catch(NullPointerException e) {
                Gdx.app.log("Type Unknown", "");
            }

        }
    }
}

