package com.roll.editor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.roll.Handlers.GlobalData;
import com.roll.entities.MySprite;

import java.util.Iterator;

/**
 * Created by Trent on 6/14/2014 for ${PROJECT_NAME}
 */
public class HudStage extends Stage {
    private static final float VIEW_PORT_WIDTH = 400;
    private static final float VIEW_PORT_HEIGHT = 300;

    Gson gson;

    Table addButtons;
    Table levelInteraction;

    //My Buttons
    MySprite wallButton;
    MySprite holeButton;

    //TextButtons
    TextButton resetCameraButton;
    TextButton exportLevel;
    TextButton importLevel;
    TextButton deleteObject;
    TextButton emptyScreen;

    //TODO use the asset manager
    Texture wallButtonTexture = new Texture(Gdx.files.internal("wallButton.png"));
    Texture holeButtonTexture = new Texture(Gdx.files.internal("holeButton.png"));

    //TODO use the asset manager
    final Skin _SKIN = GlobalData.getInstance().mySkin;

    OrthographicCamera hudCamera;

    LevelEditor levelEditor;

    private boolean dialogAlreadyExists = false;
    public HudStage() {

    }
    public HudStage(LevelEditor editor) {
        levelEditor = editor;
        hudCamera = new OrthographicCamera();
        hudCamera.setToOrtho(false, VIEW_PORT_WIDTH, VIEW_PORT_HEIGHT);
        StretchViewport viewport =  new StretchViewport(VIEW_PORT_WIDTH,VIEW_PORT_HEIGHT,hudCamera);
        viewport.setWorldSize(VIEW_PORT_WIDTH, VIEW_PORT_HEIGHT);

        //This is the line that I needed most of all.
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        setViewport(viewport);

        setupButtons();
        setupTable();
//        showSaveDialog();
    }

    private void setupTable() {
        addButtons = new Table();
        addButtons.setSize(VIEW_PORT_WIDTH, VIEW_PORT_HEIGHT);
        addButtons.top();
        addButtons.add(wallButton);
        addButtons.add(holeButton);

        levelInteraction = new Table();
        levelInteraction.setSize(VIEW_PORT_WIDTH, VIEW_PORT_HEIGHT);
        levelInteraction.bottom();
        levelInteraction.add(resetCameraButton).right().bottom().expandY();
        levelInteraction.add(exportLevel).right().bottom();
        levelInteraction.add(importLevel).right().bottom();
        levelInteraction.add(deleteObject).right().bottom();
        levelInteraction.add(emptyScreen).right().bottom();

        addActor(addButtons);
        addActor(levelInteraction);
    }

    public void showSaveDialog() {
        if(!dialogAlreadyExists) {
            this.addActor(new Window("", _SKIN) {
                final TextField fileName = new TextField("", _SKIN);
                {
                    this.debug();
                    fileName.setText("testLevel");
                    //Clear the actions?
                    this.setFillParent(true);
//                    center();

                    TextButton okButton = new TextButton("OK", _SKIN);
                    okButton.addListener(new InputListener() {
                        @Override
                        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                            hideAndSave();
                            return true;
                        }
                    });

                    TextButton cancelButton = new TextButton("CANCEL", _SKIN);
                    cancelButton.addListener(new InputListener() {
                        @Override
                        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                            hide();
                            return true;
                        }
                    });

                    this.add(new Label("Save this level as? ", _SKIN));
                    this.add(fileName);

                    row();
                    this.add(okButton).width(75).right();
                    this.add(cancelButton).width(75).left();
                }

                private void hide() {
                    this.remove();
                    dialogAlreadyExists = false;
                }
                private void hideAndSave() {
                    if(fileName.getText().equals(""))
                    {
                        this.setTitle("Please enter a name.");
                    }
                    else {
                        writeLevel(fileName.getText());
                        hide();
                    }
                }

                @Override
                public void draw(Batch batch, float parentAlpha) {
                    super.draw(batch, parentAlpha);
                    drawDebug(getRoot().getStage());
                }
            });
            dialogAlreadyExists = true;
        }
    }
    public void showLoadDialog() {
        this.addActor(new Window("Load", _SKIN) {
            {
                setFillParent(true);

                TextButton okButton = new TextButton("OK", _SKIN);
                TextButton cancelButton = new TextButton("CANCEL", _SKIN);
                cancelButton.addListener(new InputListener() {
                    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                        return true;
                    }
                    public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                        hide();
                    }
                });

                FileHandle dirHandler = Gdx.files.external("data/roll.it");
                if(dirHandler.list().length == 0 ) {
                    Gdx.app.log("Empty","Nothing to report.");
                    add(new Label("There are no levels currently saved here.", _SKIN));
                    row();
                }
                else {
                    //I just set the list of the items.
                    final List<String> objects = new List<String>(_SKIN);
                    Array<String> names = new Array<String>();

                    for(int i = 0; i < dirHandler.list().length; i ++) {
                        names.add(dirHandler.list()[i].nameWithoutExtension());
                    }
                    Gdx.app.log("names", names.toString());
                    objects.setItems(names);

                    okButton.addListener(new InputListener() {
                        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                            return true;
                        }
                        public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                            String loadThisLevel = objects.getSelected();

                            //Empty out the editorStage
                            levelEditor.getEditorStage().clear();

                            //Add the level objects in to the editor.
                            levelEditor.getEditorStage().readLevel(loadThisLevel);

                            //Close this dialog box.
                            hide();
                        }
                    });
                    add(objects);
                    row();
                    add(okButton);
                }

                //Always make sure the dialog can exit.
                add(cancelButton);
            }

            private void hide() {
                this.remove();
            }
        });
    }

    private void setupButtons() {
        emptyScreen = new TextButton("Clear", _SKIN);
        emptyScreen.addListener(new InputListener() {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                levelEditor.getEditorStage().clear();
                return true;
            }
        });

        deleteObject = new TextButton("Delete (N)", _SKIN);
        deleteObject.addListener(new InputListener() {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if(levelEditor.getEditorStage().shouldDelete()) {
                    deleteObject.setText("Delete (N)");
                    levelEditor.getEditorStage().setDelete(false);
                }
                else {
                    deleteObject.setText("Delete (Y)");
                    levelEditor.getEditorStage().setDelete(true);
                }
                return true;
            }
        });

        exportLevel = new TextButton("Export",_SKIN);
        exportLevel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                showSaveDialog();
            }
        });

        importLevel = new TextButton("Import", _SKIN);
        importLevel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                showLoadDialog();
            }
        });

        resetCameraButton = new TextButton("Reset Camera",_SKIN);
        resetCameraButton.addListener(new InputListener() {
            @Override
             public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                levelEditor.getEditorStage().resetPosition();
                return true;
            }
        });

        wallButton = new MySprite(wallButtonTexture);
        wallButton.setTouchable(Touchable.enabled);
        wallButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                final MySprite sprite = levelEditor.getEditorStage().addWall();
                sprite.setPosition(levelEditor.getEditorStage().getViewport().getCamera().position.x / 2, levelEditor.getEditorStage().getViewport().getCamera().position.y / 2);
                levelEditor.getEditorStage().addActor(sprite);
                return true;
            }
        });

        holeButton = new MySprite(holeButtonTexture);
        holeButton.setTouchable(Touchable.enabled);
        holeButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                final MySprite sprite = levelEditor.getEditorStage().addHole();
                sprite.setPosition(levelEditor.getEditorStage().getViewport().getCamera().position.x / 2 , levelEditor.getEditorStage().getViewport().getCamera().position.y / 2);
                levelEditor.getEditorStage().addActor(sprite);
                return true;
            }
        });
    }

    private void writeLevel() {
        gson = new GsonBuilder().setPrettyPrinting().create();

        Iterator<Actor> actorsInEditorStage = levelEditor.getEditorStage().getActors().iterator();
        Array<jsonActor> convertedActors = new Array<jsonActor>();

        while(actorsInEditorStage.hasNext()) {

            Actor actorInUse = actorsInEditorStage.next();
            jsonActor newActor = new jsonActor();
            newActor.height = actorInUse.getHeight();
            newActor.width = actorInUse.getWidth();
            newActor.position = levelEditor.getEditorStage().editorCamera.project(new Vector3(actorInUse.getX(), actorInUse.getY(), 0.0f));

            String name = actorInUse.getName();
            if(name == "wall" )
            {
                newActor.type = Type.WALL;
            }
            else if ( name == "start" ) {
                newActor.type = Type.START;
            }
            else if (name == "end" ) {
                newActor.type = Type.END;
            }
            else if (name == "hole" ) {
                newActor.type = Type.HOLE;
            }

            convertedActors.add(newActor);
        }

        Json json = new Json();
        json.setElementType(Array.class, "items", jsonActor.class);
        String levelObjectsAsJson = json.toJson(convertedActors);
        String name = "data/roll.it/";

        FileHandle file = Gdx.files.external("data/roll.it/second.json");

        file.writeString(json.prettyPrint(levelObjectsAsJson), false);
    }
    private void writeLevel(String levelName) {
        gson = new GsonBuilder().setPrettyPrinting().create();

        Iterator<Actor> actorsInEditorStage = levelEditor.getEditorStage().getActors().iterator();
        Array<jsonActor> convertedActors = new Array<jsonActor>();

        while(actorsInEditorStage.hasNext()) {

            Actor actorInUse = actorsInEditorStage.next();
            jsonActor newActor = new jsonActor();
            newActor.height = actorInUse.getHeight();
            newActor.width = actorInUse.getWidth();
            newActor.position = levelEditor.getEditorStage().editorCamera.project(new Vector3(actorInUse.getX(), actorInUse.getY(), 0.0f));

            String name = actorInUse.getName();
            if(name.toLowerCase().equals("wall"))
            {
                newActor.type = Type.WALL;
            }
            else if ( name.toLowerCase().equals("start")) {
                newActor.type = Type.START;
            }
            else if (name.toLowerCase().equals("end")) {
                newActor.type = Type.END;
            }
            else if (name.toLowerCase().equals("hole")) {
                newActor.type = Type.HOLE;
            }

            convertedActors.add(newActor);
        }

        Json json = new Json();
        json.setElementType(Array.class, "items", jsonActor.class);
        String levelObjectsAsJson = json.toJson(convertedActors);
        String name = "data/roll.it/" + levelName + ".json";

        FileHandle file = Gdx.files.external(name);

        file.writeString(json.prettyPrint(levelObjectsAsJson), false);
    }

    @Override
    public void draw() {
        //Make sure the camera is up to date.
        hudCamera.update();

        //Draw all actors that are a part of the hud.
        super.draw();
    }

    public class jsonActor {
        public Type type;
        public float width;
        public float height;
        public Vector3 position;
    }
    public enum Type {
        START, END, WALL, PLAYER, HOLE
    }
}