package com.roll.editor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Trent on 6/13/2014.
 */
public class LevelEditor extends Stage {
    private EditorStage editorStage;
    private HudStage hudStage;

    InputMultiplexer myInputHandler;

    public LevelEditor() {
        editorStage = new EditorStage();
        hudStage = new HudStage(this);

        myInputHandler = new InputMultiplexer();
        myInputHandler.addProcessor(0,hudStage);
        myInputHandler.addProcessor(1, editorStage);
        myInputHandler.addProcessor(2,new GestureDetector(editorStage.gestureHandler));
    }
    public EditorStage getEditorStage() {
        return editorStage;
    }
    @Override
    public void draw() {
        Gdx.input.setInputProcessor(myInputHandler);
        editorStage.draw();
        hudStage.draw();
    }
}
